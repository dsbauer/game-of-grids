


Example of `groundtypes` index:

```
groundtypes: {
	'=': {name:'floor', sound:'thud'},
	'-': {name:'floor', sound:'creak'},
	'~': {name:'water', sound:'splash'}
}
```

Here are two rooms which are coded differently but produce exactly the same result:
a 3x3 floor with a creaky floorboard in the middle.

```
	attic: {
		minimap: [
			"===",
			"=-=",
			"==="
		]
	},
	cellar: {
		minimap: [
			"===",
			"===", {1:{sound:'creak'}},
			"==="
		]
	}
	```

The `attic` uses two groundtypes, a basic floor ('=') plus a special groundtype ('-') in the middle which has different default value for property `sound`.  The `cellar` uses only the basic floor type but customizes the center space with a detail which overrides the default `sound`.


There are three categories of properties, from lowest to highest priority:

1) default: default properties are implied by the map letter for a space.  All spaces with that letter are given the same default properties, which are defined in the `groundtypes` object.

Default properties are written first.

2) details:

Detail properties are written second and overwrite default properties of the same name.
They can be used to customize a particular space to make it different from others with the same letter.
Details are placed "inline" in a room plan, following the letter row containing the detailed space.


3) changes:

As the player moves around and interacts with the world, all changes to spaces are recorded in the object `game.changes`, which is included in the savegame file.
