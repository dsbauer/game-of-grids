//Copyright 2019-2020 by A and D
/*** Terrain definitions and maps ***/

const gridwidth = 8; //for 8X8 square rooms
const startingRoomName = "shore";
const RoomPlans = {
  cliffs: {
    weather:"snow:.20:50000",
    minimap:[
    "..b(.r)b",
    "b(^]..[^",
    "(]T.@.r.",
    "]b.b*...",
    "t.(^^^)T", //{4:{name:'cliffBreak'}},
    ".(C.bt)b", {2:{teleports:'grotto:24'}},
    "b(bb.b)t",
    "^].t.T[^"],
    treasuremap:
    "$/ p  $ "+
    "        "+
    "        "+
    "    /   "+
    "        "+
    "        "+
    "!     / "+
    "  #     ",
    east:'lake'
  },
  towerFloor: {
    minimap: [
    "~~~~~~~~",
    "~~%%%%%~",
    "~~%...%~",
    "~r'..+`~",
    "~r%.*.%~",
    ".t.%d%%~", {4:{teleports:'cape'}},
    "r.b....~",
    "rr......"],
    treasuremap:
    "        "+
    "        "+
    "   g$   "+
    "        "+
    "        "+
    "        "+
    "        "+
    "        ",
    up:'towerMiddle'
  },
  towerMiddle: {
    minimap: [
    "~~~~~~~~",
    "~~%%%%%~",
    "~~%___%~",
    "~~%__#%~", {4:{light:.6, lit:true}},
    "~~%___%~",
    "..%%%%%~",
    "........",
    "........"],
    treasuremap:
    "        "+
    "        "+
    "        "+
    "        "+
    "   B    "+
    "        "+
    "        "+
    "        ",
    up:'towerTop',
    down:'towerFloor'
  },
  towerTop: {
    weather:true,
    minimap: [
    "~~~~~~~~",
    "~~~~~~~~",
    "~~%___%~",
    "~~%_v=%~",
    "~~%___%~",
    "..%%%%%~",
    "........",
    "........"],
    treasuremap:
    "        "+
    "        "+
    "        "+
    "        "+
    "   f    "+
    "        "+
    "        "+
    "        ",
    down:'towerMiddle'
  },
  cape: {
    weather:true,
    sound:'ocean',
    daylight:.1,
    minimap: [
    "~~~~~~~~",
    "Wr~~~~r~", {0: {teleports:'flotsam'}},
    "~~r~L~~~", {4: {entryFrom:'s',teleports:'towerFloor'}},
    "~~~r*~~~",
    "~~~..~~~",
    "~~~..~~~",
    "~~r.brt~",
    "~r..r..~"],
    //teleportTargets: ['flotsam','towerFloor'],
    treasuremap:
    "        "+
    "        "+
    "        "+
    "        "+
    "        "+
    "        "+
    "        "+
    "      / ",
    south:"lake",
    east:"northWoods"
  },
  flotsam: {
    minimap:[
      "~~~~~~~~",
      "~~~~~~~~",
      "~~~~~~~r",
      "_~~~~r~~",
      "__~~~_~r",
      "~_~~~__r",
      "~~~~~~__",
      "~~~~~~~~"],
    treasuremap:
      "        "+
      "   /    "+
      "     /  "+
      "/     $ "+
      "  $/  / "+
      "$     $ "+
      "     / $"+
      "/       ",
    north:"cape:0",
    south:'cape:16',
    west:'cape:16'
  },
  lake: {
    weather:true,
    sound:'water',
    daylight:.1,
    minimap: [
    "t..[C^)r", {4: {teleports:"pitCave:1"}},
    "b.~~.~)t",
    ".~~~~~b.",
    ".-.*h~~.", {4: {entryFrom:'w', teleports:'hut:34'}},
    "r~~~~~..",
    "(^>|[>.<", {3: {entryFrom:'w', teleports:'grotto:59'}},
    "]Tb~....",
    "(bl-lt.r"],
    treasuremap:
    "        "+
    "  A     "+
    "        "+
    "        "+
    "        "+
    "        "+
    "        "+
    "$ /   / ",
    east:"woods",
    north:"cape",
    west:"cliffs"
  },
  hut: {
    minimap: [
    ".~~~~~~~",
    "..%%%%.~",
    ".%%&.%%~",
    ".%f...%~",
    ".D..=.%~", {1:{locked:'blue', teleports:'lake:27'}},
    ".%....%~",
    ".%%..`%~",
    "..%%%%.."],
    treasuremap:
    "        "+
    "        "+
    "        "+
    "  l  r  "+
    "        "+
    "        "+
    "   $    "+
    "        ",
    down:"cellar",
    smoothType:'stonewall'
  },
  cellar: {
    sound:'dungeon',
    daylight:.01,
    minimap: [
    "%%%%%%%%",
    "%%...%%%",
    "%%%%D%%%", {4:{locked:'red'}},
    "%%%...`%",
    "%%'.+.%%",
    "%%%...`%",
    "%%%%%...",
    "%%%%%%%%"],
    treasuremap:
    "        "+
    "  y$    "+
    "        "+
    "        "+
    "        "+
    "        "+
    "        "+
    "        ",
    up:"hut",
    east:"tunnel",
    smoothType:'stonewall'
  },
  tunnel: {
    sound:'dungeon',
    daylight:.01,
    minimap: [
    "%%%D%%%%", {3:{locked:'yellow', teleports:'shore'}},
    "%V..`%+%", {1:{price:3, loot:'battery',  icon:'\u{1f50b}'}},
    "%V..%% `", {1:{price:7, loot:'pickaxe',  icon:'\u26CF'}, 7:{status:'burning',lit:true}},
    "%V..%%.%", {1:{price:4, loot:'umbrella', icon:'\u2602'}},
    "%%%D%..%", {3:{locked:'green'}},
    "%%'...%%",
    "....%%%%",
    "%%%%%%%%"],
    treasuremap:
    "        "+
    "        "+
    "  $     "+
    "        "+
    "        "+
    "        "+
    "        "+
    "        ",
    west:"cellar",
    up:"woods",
    smoothType:"stonewall"
  },
  woods: {
    weather:true,
    sound:'forest',
    minimap: [
    "T.t.tb.t",
    "tb.btb=t",
    "Tt.tT@t.",
    ".bb.btTt",
    ".tTbtT.b",
    "Tt.bb.t.",
    ".tT.btb.",
    ".bbt.bbt"],
    treasuremap:
    "        "+
    "        "+
    "        "+
    "        "+
    "      / "+
    "  /     "+
    "        "+
    "        ",
    down:"tunnel",
    west:"lake",
    east:"shore",
    north:"northWoods",
    south:"southShore"
  },
  northWoods: {
    weather:"clear:0",
    sound:'forest',
    daylight:.1,
    minimap: [
    "~~~~~~~~",
    "~~~~~~~~",
    "~Trt.l~~",
    "~~...b.~",
    "~lbtbt.r",
    "~t..tTbt",
    "rOb.t..r", {1:{teleports:'pitCave:21'}},
    "r.T.Tb.t"],
    treasuremap:
    "        "+
    "        "+
    "        "+
    "        "+
    " /      "+
    "        "+
    "        "+
    "        ",
    south:"woods",
    west:"cape",
    east:'northShore'
  },
  shore: {
    weather:true,
    sound:'ocean',
    minimap: [
    "rbb~~~~~",
    "t....~~~",
    ".b....~~",
    "r.@*..~~",
    "....~~~~",
    "b....~~~",
    "....~~~~",
    "Tr~~~~~~"],
    west:"woods",
    north:"northShore",
    south:'southShore:15'
  },
  northShore: {
    weather:true,
    sound:'ocean',
    daylight:.1,
    minimap: [
    "~~~~~~~~",
    "~~~~~~~~",
    "~~~.h.~~", {4:{entryFrom:'ws', teleports:'ruin'}},
    "~~..*~~~",
    "~~I~~~~~", {2:{teleports:'underbridge'}},
    "~~..~~~~",
    "~b..r~~~",
    "tbb~~~~~"],
    south:"shore",
    west:"northWoods"
  },
  underbridge: {
    minimap: [
      "~88%%88.",
      "~r8%%8~~", {6:{treasure:'coin'}},
      "~~~rr~.~",
      "~~~~~r~~",
      "~r.~~~~~", {2:{treasure:'coin'}},
      "~~~~~r~~",
      "~~8%%8r.",
      "~88%%88."
    ],
    west: "northShore:33",
    east: "northShore:35"
  },
  ruin: {
    weather:true,
    sound:'ocean',
    daylight:.1,
    minimap: [
    "~~~~~~~~",
    "~.%.%..~",
    "~.t...%~",
    "~~'....b",
    ".rb...%.",
    "..%%d%%.", {4:{teleports:'northShore'}},
    "t......r",
    "........"],
    //teleportTargets:["northShore"],
    treasuremap:
    "        "+
    "   /  b "+
    "        "+
    "        "+
    "        "+
    "       $"+
    "        "+
    "        ",
  },
  southShore: {
    weather:true,
    sound:'ocean',
    daylight:.1,
    minimap: [
    "t.....*t",
    "~...lt.~",
    "~...b..~",
    "~..r...~",
    "~......~",
    "~~...~~~",
    "~~~~~~~~",
    "~~~~~~~~"],
    treasuremap:
    "        "+
    "        "+
    "     /  "+
    "A       "+
    "   $    "+
    "        "+
    "        "+
    "        ",
    north:"woods",
    east:"shore:58"

  },
  grotto: {
    sound:"waterfall",
    minimap: [
    "88:88888",
    "88.88888",
    "88.8.888",
    "(.....|8",
    "888...|.",
    "888...88",
    "8888|888",
    "888b|888", {4:{light:.5, lit:true}}],
    treasuremap:
    "        "+
    "  $     "+
    "        "+
    " $      "+
    "        "+
    "        "+
    "        "+
    "        ",
    north:"secretCave",
    south:"lake:42",
    east:"pitCave",
    west:"cliffs:50",
    smoothType:'cavewall',
    bridgeType:'rubble' //avoids rounding passage
  },
  pitCave: {
    sound:'dungeon',
    minimap: [
    "8.888888",
    "8.88..88",
    "8......8", {5: {light:.6, lit:true}},
    "88...U.8",
    "..8...88",
    "8....888",
    "8888.888",
    "88888888"],
    treasuremap:
    "        "+
    "        "+
    "        "+
    "        "+
    "        "+
    "        "+
    "    $   "+
    "        ",
    north:"lake:12",
    west:"grotto",
    up:"northWoods:57",
    smoothType:'cavewall'
  },
  secretCave: {
    sound:'cave1',
    minimap: [
    "88888888",
    "88888888",
    "888...88",
    "88.....8", {4:{light:.6, lit:true}},
    "888...88",
    "888..888",
    "88..8888",
    "88.88888"],
    treasuremap:
    "        "+
    "        "+
    "        "+
    "    s   "+
    "        "+
    "        "+
    "        "+
    "        ",
    south:"grotto",
    smoothType:'cavewall'
  }
};

// Constuctors for categories which need prototypes:

// Base class for all item types
class LootItem {
  constructor(details) {
    // copy any details into instance properties
    extend(this,details);
    // store the constructor's name (e.g. "LootItem") as a de-capitalized instance property ("lootItem")
    this.name=this.constructor.name[0].toLowerCase() + this.constructor.name.substr(1);
  }
}

class LightSource extends LootItem {
  constructor() {super(); this.lit = true;}
}

class LanternLit extends LightSource {
  constructor() {super();}
}
LanternLit.prototype.light = .7;

class FlashlightLit extends LightSource {
  constructor() {super();}
}
FlashlightLit.prototype.light = {glow:.28, beam:.4, width:70};

ctorIndex = {
  lanternLit:LanternLit,
  flashlightLit:FlashlightLit
}

const treasuretypes = {
"/": "stick",
"F": FlashlightLit,//already lit
"f": "flashlight",
"L": LanternLit,//already lit
"l": "lantern",
"b": "blue-key",
"r": "red-key",
"y": "yellow-key",
"g": "green-key",
"$": "coin",
"p": "pickaxe",
"s": "statue",
"B": "boots",
"A": {name: "rowboat", vehicle:"water"},
// Climbing gear on ground:
"#": "ladder",
"@": "rope",
// Gear deployed for climbing:
// These don't require their own symbols; deployed property can be an inline detail in map
"|": {name: "ladder", deployed:true},
"!": {name: "rope", deployed:true}
}

var climbQualifiers = {s: //when trying to climb from south,
  // one of these functions must be true:
  [moveQualifiers.ropeAbove, moveQualifiers.ladderBelow]};

const groundtypes = {
".": {name: ""},
"h": {name: "house"},
"H": {name: "house2"},
"W": {name: "shipwreck", barrier:'water'},
"L": {name: "lighthouse", lit:true, light:{beam:1, angle:0, yoffset:-.25}},
"v": {name: "spotlight", barrier:true, status:'burning', lit:true, light:{beam:1, angle:0, yoffset:.3}},
"O": {name: "openpit"},
"r": {name: "rock", barrier: true},
"t": {name: "tree", barrier: true},
"T": {name: "tree2", barrier: true},
"~": {name: "water", barrier: 'water'},
"b": {name: "bush"},
"g": {name: "grass"},
"|": {name: "waterfall", status:"quenching"},
"#": {name: "ladder", climbup: true, climbdown: true},
"+": {name: "ladderUp", climbup: true},
"=": {name: "ladderDown", climbdown: true},
"U": {name: "haystack", climbup:true, status:'flammable', light:1, lit:false},
"_": {name: "boards", bridge:true},
"-": {name: "bridgeEW", bridge:true, teleportFromSide:"ns", barrier:"bridge"},
"I": {name: "bridgeNS", bridge:true, teleportFromSide:"we", barrier:"bridge"},
"%": {name: "stonewall", barrier: true},
"8": {name: "cavewall", barrier: true},

"^": {name: "cliff", entryFrom:"n",  climbable:true, qualifiedEntryFrom:climbQualifiers},
">": {name: "cliff", entryFrom:"ne", climbable:true, qualifiedEntryFrom:climbQualifiers, decor:"rightend"},
"<": {name: "cliff", entryFrom:"nw", climbable:true, qualifiedEntryFrom:climbQualifiers, decor:"leftend"},
"]": {name: "cliffSE", entryFrom:"n", climbable:true, qualifiedEntryFrom:climbQualifiers},
"[": {name: "cliffSW", entryFrom:"n", climbable:true, qualifiedEntryFrom:climbQualifiers},
"C": {name:"cliffCaveSE", entryFrom:"ns", teleportFromSide:"s"},
 //not yet climbable:
 "(": {name: "cliffE", entryFrom:"nsw"},
 ")": {name: "cliffW", entryFrom:"nse"},

":": {name: "rubble", barrier: true, diggable:true, bridge:true},
"&": {name: "stove", light:.7, barrier:true, status:"burning", lit:true},
"@": {name: "bonfire", light:1, status:"burning", lit:true},
"l": {name: "lamppost", light:{glow:.4, yoffset:-.4}, lit:true},
"`": {name: "stonewall", decor: "wallSconceR", barrier: true, entryFrom:'w', light:{glow:.5, xoffset:-.5}, status:"flammable"},
"'": {name: "stonewall", decor: "wallSconceL", barrier: true, entryFrom:'e', light:{glow:.5, xoffset: .5}, status:"flammable"},
"V": {name: "vendingMachine", barrier:true, status:'vending'},// needs detail: {price, loot, icon}
"f": {name: "table"},
"d": {name: "door", bridge:true}, //unlocked door
"D": {name: "door", bridge:true, barrier:true} //locked door; needs detail {locked:color}
};

/* Glossary of terrain properties...

property (possible values)
_________________________
barrier (true, <terrain>):
  If true, space is always blocked.  If <terrain>, space may be entered when riding a vehicle matching <terrain>.

bridge (true):
  When smoothing corners during display, prevent this space from rounding near water (or other background).

climbdown (true):
  Space may be climbed downward to corresponding space in room below.

climbup (true):
  Space may be climbed upward to corresponding space in room above.

climbable (true):
  Space allows climbing gear (e.g. rope, ladder) to be deployed there

decor (<class>):
  String <class> is automatically added to CSS classes for this space's DOM element, changing its appearance.

deployed (true):
  A property of an item when it has been used in certain spaces to enable climbing.

diggable (true, N):
  This barrier may be dug away N times (when holding pickaxe) before turning into floor.
  (true == 1)

entryFrom (<news>):
  String <news> is any subset of the letters 'n','e','w','s', indicating the adjacent spaces from which
  this space may be entered.  If omitted or false, equivalent to "news" --> ok from any direction.

icon (<unicode>):
  String <unicode> (usually a single character) labels a vending machine with a simple icon
  showing which loot it dispenses.

light (R):
  The description of light emitted by this space when lit.
  If R is a number, it should be 0..1 (assumed glow).
  Otherwise it should be an object, with properties including:
    glow (0..1): brightness of halo
    beam (0..1): brightness of beam
    angle (degrees): orientation of beam (0= north)
    xoffset, yoffset (pixels):adjust origin of light relative to space center

lit (true):
  When true, space emits light as described by light property

locked (<color>)
  String <color> corresponds to the name of the key needed to unlock this door.

loot (<item>):
  String <item> is the name of the loot (inventory object) sold by a vending machine.

price (N):
  N is a number of coins >0; the price of a vending machine item.

status (<class>)
  Like decor, status is automatically added to CSS to change space appearance.
  But status is also used in game logic, to check special effects.  Values may include:
  -- quenching: puts out lit torches
  -- flammable: lights if carrying lit torch
  -- burning: ignites unlit torches
  -- vending: sells loot for coins

teleports (<target>)
  When touched/entered, space teleports to target, string "roomname" or "roomname:slot"

vehicle (<terrain>)
  A property of loot, not of a space.  <terrain> is a string like "water"

*/
