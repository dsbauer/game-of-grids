/*
This module makes certain game elements (e.g. weather) consequential rather than merely decorative.
It tracks the player's status, a set of possible afflictions caused by the environment.
Current afflictions include:

-- Dampness of clothing and inventory.
Dyamics: Increases quickly with precipitation, decreases slowly with warm temperatures and low humidity.
Effect: makes flammable items harder to ignite; decreases personal temperature; raises stress.

-- Personal temperature.
Dynamics: Converges toward environment temperature; raised during exertion.
Effect: if too high or too low, raises stress.

-- Hunger/Thirst
Dynamics: increases with time and exertion, decreases with food/water.
Effect: raises stress.

-- Tiredness
Dynamics: increases with time and exertion, decreases with sleep.
Effect: raises stress.

-- Fear
Dynamics: increase with darkness and certain noises, decreased with certain items.
Effect: raises stress.

-- Stress: the accumulation of all afflictions.
Effect: certain difficult actions (e.g. climbing, crafting/merging items) fail if stress is too high

*/

const Status = (function(){ //module is wrapped in a function to keep components private

var defaults = { // initial values for game.dude.status:

	hungry:0,  // 0 (fed) to 100 (starving)
	thirsty:0, // 0 (hydrated) to 100 (dehydrated)
	damp:0,    // 0 (dry) to 100 (soaked)
	tired:0,   // 0 (rested) to 100 (exhausted)
	temp:0,   // -100 (very cold) to 100 (very hot)
	tempDegrees:70, // personal temperature in degress F
	// cumulative effect --> stress
	stress:0 // 0 (relaxed) to 100 (scattered)
}
const stressFactors=['damp','temp']; //which factors add to stress

function Status(data) {
	if (!data)
		data = defaults;
	extend(this,data);
	redisplayStatus(data);
}

var afflictions; // eventual alias for game.dude.status

// Move these to shared toolkit:
function clamp(input,low,high) { // ensure that input is between low and high
  if (input<=low) return low;
  if (input>=high) return high;
  return input;
}
function ramp(input,low,high) { // return 0..1, what fraction input is between low and high
	if (input<=low) return 0;
	if (input>=high) return 1;
	var range = high-low;
	return (input-low)/range;
}
function rescale(value,fromLo,fromHi,toLo,toHi) {
  return lerp(toLo,toHi,ramp(value,fromLo,fromHi));
}

// Raise or lower an affliction by a fixed amount, regardless of current level:
function incrementAffliction(name,min,max,step) {
	var current = afflictions[name] || 0;
	return afflictions[name] = clamp(current+step,min,max);
}
// Raise or lower an affliction a proportion of distance to target level:
function convergeAffliction(name,target,riseRate,fallRate) {
	var current = afflictions[name] || 0,
			delta = target - current,
			rate = (delta>0)? riseRate: fallRate,
			step = delta * (rate);
	return afflictions[name] = clamp(current+step,0,100);
}

function adjustDampness() {
	// increasing factors: weather dampness
	var dampTypes = weatherModule.getDampness(),
			humid = dampTypes.humidity, // 0, <2 (rain/snow), or <8 (fog)
			soakRate = 0;
	if (!game.dude.loot.umbrella)
		soakRate += dampTypes.precipitation;
	if (!game.dude.loot.boots)
		soakRate += dampTypes.ground;

	// diminishing factors: weather temp, (heat sources), (daytime?)
	var temp = weatherModule.getTemperature(),
			dryingRate = rescale(temp,50,80,.1,1); //--> .1 to 1
	// drying rate drops by humidity, down to 0:
	dryingRate = Math.max(0, dryingRate - humid);

	var nowdamp = incrementAffliction("damp",humid,100,soakRate-dryingRate);
	refreshStatusIcon('damp',nowdamp);
	return nowdamp;
}

function adjustPersonalTemp(exertion) {
	// factors: outside temperature, heat sources, dampness
	var damp = afflictions.damp,
			riseRate = Math.max(0, .2-(damp/50)),
			fallRate = .1+(damp/200),
			target = weatherModule.getTemperature();
	afflictions.tempDegrees += (exertion || 0);
	convergeAffliction("tempDegrees",target,riseRate,fallRate);

	// Decide if personal temp is too hot or cold:
	var deg = afflictions.tempDegrees,
			min = 65,
			max = 75,
			tooHigh = (deg>max)? deg-max: 0,
			tooLow  = (deg<min)? deg-min: 0,
			excess = (tooHigh || tooLow) * (100/30);
	afflictions.temp = clamp(excess,-100,100);
	refreshStatusIcon('temp',afflictions.temp);

	return afflictions.tempDegrees;
}

function adjustStress() {
	var maxStress = stressFactors.length,
			stress = 0,
			worst = 0,
			worstFactor = "";
	for (var i in stressFactors) {
		var name = stressFactors[i],
				level = afflictions[name],
				badness = Math.pow(level/20,2);
		if (badness>worst) {
			worst = badness;
			if (name==="temp") {
				name= (level<0)? "cold":"hot";
			}
			worstFactor = name;
		}
		stress += badness;
	}
	afflictions.worst = worstFactor;
	afflictions.stress = clamp(stress,0,100);
	refreshStatusIcon('stress',afflictions.stress);
	return afflictions.stress;
}

function adjustStatus(exertion) {
	afflictions = this;
	adjustDampness();
	adjustPersonalTemp(exertion);
	adjustStress();
}

function redisplayStatus(status) {
	for (var name in status) {
		refreshStatusIcon(name,status[name]);
	}
}

function prepareUI() {
	var toprow = document.getElementById('toprow-right'),
			icons = document.createElement('div');
	icons.id = "statusIcons";
	makeStatusIcon(icons,'damp');
	makeStatusIcon(icons,'temp');
	makeStatusIcon(icons,'stress');
	toprow.appendChild(icons);
}

function makeStatusIcon(parent,name) {
	var icon = document.createElement('div');
	icon.id = "status-"+name;
	icon.classList.add("statusIcon");
	parent.appendChild(icon);
}

function refreshStatusIcon(name,lvl) {
	var div = document.getElementById('status-'+name);
	if (!div) return;
	div.classList.toggle('negative',lvl<0);
	div.style.setProperty('--status-level',Math.abs(lvl));
}

Status.prototype.canFocus = function (maxStress,actionName) {
	if (this.stress<maxStress)
		return true;
	// else fail, offer message:
	console.log(`You try ${actionName}, but you're too ${this.worst} and can't focus right now.`);
}
Status.prototype.dryEnough = function (maxDamp) {
	if (this.damp<maxDamp)
		return true;
	console.log(`It's too wet to ignite.`);
}

Status.prepare = prepareUI;
Status.prototype.adjust = adjustStatus;
return Status;

})();
