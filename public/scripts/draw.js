//Copyright 2019-2020 by A and D
/******** Display functions *********

Some conventions in variable names:
"slot": an integer identifying the position of a space in the room, and the id of a square
Objects:
"space": the object describing one space in the room (the "Model")
"dude": the object describing the player character
"treasure": an object still on the ground
"loot": a carried object
Divs:
"square": the DOM <div> element depicting a space (the "View")
"icon": the DOM <div> element within (child of) a square depicting dude or object

This module's purpose is only to display views (squares/icons);
it should never change the underlying models (dude/space/treasure/loot).

Information redundancy:

slot <--> square
    \    /
    \   /
		v  v
   space

Either slot or square is sufficient to find the other or to find space,
but space cannot find either slot or square.  Therefore slot/square should be the primary
parameter, but functions can accept space as an optional parameter if available to the caller.

*/


const spaceWidth = 60; //size of one square, in pixels
// Beware a lot of things depend on this, but may not be automatically scaled if it changes.

function reorientDude(dude,icon) {
	//redraw dude (and personal lights) in new orientation, instead of in new location
	orientIcon(dude,icon); // maybe turn icon to face new direction
	displayPersonalLight(dude); // redirect flashlight beam
}

const orientationName = [ //index 0..3 corresponds to dude.facing #
	// CSS classes triggering div orientation
	'faceNorth',
	'faceEast',
	'faceSouth',
	'faceWest'
];

function orientIcon(object,icon) { // should have both or neither arg
	if (!object)
		object = game.dude;
	if (!icon)
		icon = document.getElementById("dude");
	// model keeps track of old orientation so class is easy to replace
	if (object.wasFacing && object.wasFacing != object.facing)
		icon.classList.remove(orientationName[object.wasFacing]);
	var nowFacing = orientationName[object.facing];
	icon.classList.add(nowFacing);
}

// Some useful mathematical tools:
function pixelX(slot,xoffset) {
  // compute horizontal pixel within grid
  var col = columnAtSlot(slot) + (xoffset || 0); //offsets measured in spaces, not pixels
  return ((col+.5)*spaceWidth);
}
function pixelY(slot,yoffset) {
  // compute vertical pixel within grid
  var row = rowAtSlot(slot) + (yoffset || 0);
  return ((row+.5)*spaceWidth);
}

function lerp(from,to,fraction,maxDist) {
  // Assuming fraction is 0..1,
  // linearly interpolate between values from and to
  var totalDistance = to-from,
      incrementalDistance = fraction*totalDistance;
  // optional maxDist should be a positive #.  If provided, limits increment magnitude.
  if (maxDist < Math.abs(incrementalDistance)) {
    incrementalDistance = maxDist * Math.sign(totalDistance);
  }
  return (from + incrementalDistance);  //May return non-integer
}

function lerpInteger(from,to,fraction) {
  // Assuming fraction is 0..1,
  // interpolate to an integer between from and to
  var distance = to-from;
  return Math.floor(from + fraction*distance);  //Always returns integer
}

function displayDude(startClean) {
	var icon =  document.getElementById("dude");
  if (!icon){
		icon = document.createElement("div");
  	icon.id = "dude";
    icon.className = game.dude.skin;
  }
	if (game.dude.vehicle)
		icon.classList.add(game.dude.vehicle.name);
	reorientDude(game.dude,icon);

  //img.className = world[slot] + " dude";
  var square = findSquare(game.dude.slot);
  if (startClean)
    square.innerHTML = ""; //clear contents, including loot and objects
  square.appendChild(icon);
}

function displayTreasure(treasure,square) { //square may be a slot # instead
	if (!treasure)
		return;

  var icon = document.createElement("div");
  icon.className = "treasure "+(treasure.name || treasure);
  decorateItem(icon,treasure);
	orientIcon(treasure,icon);
	square = ensureSquare(square);
  square.appendChild(icon);
}

function displaySpace(slot, grid, spaces, lighting){
	// Draws 1 square on the map
	var square = document.createElement("div");
  square.id = slot;
  var space = spaces[slot];
	//var light = lighting && lighting[slot] || 0;
	//light+=lighting.ambient;
	//if (light)
		//illuminateSpace(square,light);
	var glows = (space.lit && space.light) ||
      (space.treasure && space.treasure.lit && space.treasure.light);
	if (glows)
		addLightSource(slot,glows);

  decorateSpace(space, square);
  displayTreasure(space.treasure, square);
  grid.appendChild(square);
}

// For space-local lighting:
// function illuminateSpace(div,light) {
// 	div.style.opacity = light;
// 	//div.style.filter = `brightness(${Math.sqrt(light)})`
// }

function addClass(div,value) {
	if (value)
		div.classList.add(value);
}
function addKey(div,key,obj) {
  if (obj[key])
    div.classList.add(key)
}
function removeClass(div,object,changes,property) {
	if (property in changes) { //if property is changing,
		var oldvalue = object[property];// and its old value isnt empty,
		if (oldvalue)
			div.classList.remove(oldvalue);//remove it from classes
	}
}
function addAttribute(div,name,value) {
	if (value)
		div.setAttribute(name,value);
}
function decorateSpace(space, square) {
	square = ensureSquare(square);
	addClass(square,"floor");
	addClass(square,space.name);
	addClass(square,space.status);
	addClass(square,space.decor);
	addClass(square,space.locked);
	addAttribute(square,'price',space.price);
	addAttribute(square,'icon',space.icon);
}
function decorateItem(icon,item){
  if (item && typeof item === "object") {
    addKey(icon,'lit',item);
    addKey(icon,'deployed',item);
  }
}
function undecorateSpace(space, square, changes) {
	// space still holds old values, some of which are about to be changed via changes.
	// The old values need to be removed from div's classes.
	// Don't try removing all classes; some are needed for non-model purposes (e.g. smoothing masks)
	square = ensureSquare(square);
	removeClass(square,space,changes,'name');
	removeClass(square,space,changes,'status');
	removeClass(square,space,changes,'decor');
	removeClass(square,space,changes,'locked');
}

function findSquare(slot) {
  return document.getElementById(slot);
}
function ensureSquare(squareOrSlot) {
	if (squareOrSlot instanceof HTMLElement)
		return squareOrSlot;
	return findSquare(squareOrSlot)
}

function redisplaySpace(slot, space) {
  decorateSpace(space, findSquare(slot))
}

function smoothSpace(slot,isWet,isDry,smoothName) {
	// Smooth water edges at slot.
	// "water" can also be any other smoothable texture (eg. walls)
	// "wet" here means essentially "of type smoothName"
	// Call only after creating all spaces in room and rendering at least this space.

  var spaces = game.room.spaces,
			div = findSquare(slot);
			here = spaces[slot],
			north = spaces[slot-gridwidth],
			south = spaces[slot+gridwidth],
			hasWest = (slot%gridwidth),
			hasEast = ((slot+1)%gridwidth),
			west = hasWest && spaces[slot-1],
			east = hasEast && spaces[slot+1];

  if (isWet(here)) {
		// For water spaces, round any corners which are dry (adjacent to 3 non-water)
		var nw = hasWest && spaces[slot-gridwidth-1],
				ne = hasEast && spaces[slot-gridwidth+1],
				sw = hasWest && spaces[slot+gridwidth-1],
				se = hasEast && spaces[slot+gridwidth+1];
		// If a corner is dry, clip it from water space to reveal dry background:
		if (isDry(north) && isDry(west) && isDry(nw))
			div.classList.add('clipNW');
		if (isDry(north) && isDry(east) && isDry(ne))
			div.classList.add('clipNE');
		if (isDry(south) && isDry(east) && isDry(se))
			div.classList.add('clipSE');
		if (isDry(south) && isDry(west) && isDry(sw))
			div.classList.add('clipSW');
  } else if (isDry(here)) { //excludes bridges
		// For dry spaces, visually round any wet corners
		// which means overlaying a mask of 4 wet corners and clipping non-wet corners from it
		var wetNW = isWet(north) && isWet(west),
				wetNE = isWet(north) && isWet(east),
				wetSE = isWet(south) && isWet(east),
				wetSW = isWet(south) && isWet(west);
		if (wetNW || wetNE || wetSE || wetSW) { //only if at least one wet corner...
			div.classList.add('masked'); //creates 4-cornered mask
			div.classList.add(smoothName+'-mask'); //determines texture of visible "wet" corners
			// If a corner is not wet, clip it from mask:
			if (!wetNW) div.classList.add('clipNW');
			if (!wetNE) div.classList.add('clipNE');
			if (!wetSE) div.classList.add('clipSE');
			if (!wetSW) div.classList.add('clipSW');
		}
  }
}

function smoothCorners(spaces,waterName) {
	if (!waterName)
		waterName = 'water'; //could be other terrain specified in room plan
	//if (!bridgeName)
	//	bridgeName = 'boards';
	// Create functions for smoothing criteria:
	function isWet(space) {
		return space && space.name==waterName;
	}
	function isDry(space) {
		return space &&
						space.name!==waterName &&
						!space.bridge;
						//space.name!==bridgeName; // boards are wet so that water appears to flow under bridges
	}
	var count = 0;
  while (count<spaces.length) {
		// smooth each space according to criteria:
		smoothSpace(count,isWet,isDry,waterName);
    count = count + 1;
  }
}

function preparePortal() {
  // Clear portal and insert blank grid element
  var portal = document.getElementById('portal');
  portal.innerHTML = ''; //clear contents
  var grid = document.createElement("div");
  grid.id = "grid";
  portal.appendChild(grid);
}

function prepareSidebar() {
  var basket = document.getElementById('loot');
  basket.addEventListener('click',makeSelectionToggle());
}

function displayGrid(room) {
	var grid = document.getElementById("grid"),
      portal = document.getElementById("portal");

  grid.innerHTML = "";
  grid.className = room.plan.backgroundColor || 'dirt';
	makeDarkness();

  //renderDarkness(grid,room);

  var slot = 0;
  while (slot<room.spaces.length)
	{
		displaySpace(slot, grid, room.spaces, room.lighting);
  	slot = slot + 1;
	}
	//Optional water smoothing:
	smoothCorners(room.spaces,room.plan.smoothType);

	if (typeof weatherModule!=='undefined') {
		if (room.plan.weather)
			weatherModule.resumeWeather(room.plan.weather);
		else
			weatherModule.pauseWeather();
	}

  renderDarkness(portal,room);
  displayDude();
	startAmbience(room);
}

function makeSelectionToggle(name,div) {
  return function(evt) {
    toggleSelection(name,div);
    evt.stopPropagation();
  }
}

function makeTargetButton(itemDiv,name) {
  var targetBtn = document.createElement('div');
  targetBtn.className = "targetThis";
  targetBtn.addEventListener('click',function(evt){
    useItemWithItem(name);
    evt.stopPropagation();
  });
  itemDiv.appendChild(targetBtn);
}

function displayLootItem(item,amount,div) {
	// render one loot item, adding it if new, or remove it (if amount==0)
  var name;
  if (typeof item === "object") { //item is actual object
    name = item.name;
  } else { // item is just a name
    name = item;
    item = game.dude.loot[name];
  }
	if (!div)
		div = document.getElementById('my_'+name);
  if (typeof amount==="object")
    amount = 1;
	if (!div && amount) { //still not found, make one
		div = document.createElement("div");
		div.id = 'my_'+name;
		div.className = "lootItem "+name;
    decorateItem(div,item);
    div.addEventListener('click',makeSelectionToggle(name,div));
	}

	var unitsLeft = Math.ceil(amount),
			fraction = amount-Math.floor(amount);
	if (!unitsLeft) {
		div.remove();
	} else {
		div.innerHTML = (unitsLeft>1)? `x<b>${unitsLeft}</b>`: ''; //if multiple, label with how many
		if (fraction) {
			// adjust loot icon downward according to remaining fraction
			div.style.backgroundPosition = 'center '+(-fraction*50+25)+'px'
		}
		// make sure it's attached to loot basket
		if (!div.parentElement) {
			var basket = document.getElementById('loot');
			basket.appendChild(div);
		}
	}
  makeTargetButton(div,name);
}
function displayAllLoot() {
  var basket = document.getElementById("loot");
  basket.innerHTML = ""; //empty basket and redraw everything
  var loot = game.dude.loot;
  for (var name in loot) {
		displayLootItem(name,loot[name]);
  }
}

function refreshTransferButton(canDrop) {
  var arrow = document.getElementById('transferButton');
  if (arrow) {
    arrow.classList.toggle('reversed',canDrop);
  }
}

function toggleSelection(name,div) {
  var basket = document.getElementById('loot');

  // unselect previous selection, if any
  if (gui.selectedItem) {
    gui.selectedItem.div.classList.remove('selected');
  }

  if (!name || // no name, or
    gui.selectedItem && gui.selectedItem.name === name // same name as before
  ) {
    // select nothing
    gui.selectedItem = null;
    basket.classList.toggle("selection",false);
    refreshTransferButton(false);
  } else {
    // select new item
    gui.selectedItem = {
      name:name,
      div:div
    }
    gui.selectedItem.div.classList.add('selected');
    basket.classList.toggle("selection",true);
    refreshTransferButton(canDropHere(name));
  }
}

var gui = {}; //tracks state of inferface
