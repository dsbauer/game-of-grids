//Copyright 2019-2020 by A and D
/**** Math for moving around grid ****/

function SlotAtRowCol(row,col) {
//Calculates the "slot" # of a row and column.
//"Slot #" uniqely identifies a space on the map. It stars at 0 in the top left and ends at 63 in the bottom right on an 8 by 8 grid.
	return row * gridwidth + col;
}
function rowAtSlot(slot) {
	return Math.floor(slot / gridwidth);
}

function columnAtSlot(slot) {
	return slot % gridwidth;
}

/******* Movement *******/
function buildResult(newRow, newCol, edge, angle) {
	var result = {
  	newSlot: SlotAtRowCol(newRow, newCol),
  	edge: edge,
		angle: angle
  };
	return result
}

function moveNorth(slot) {
  var edge = "";
  var newRow = rowAtSlot(slot) - 1;
  if (newRow < 0) {
 	  //uh-oh
    newRow = (gridwidth - 1)
    edge = "north";
  }
  return  buildResult(newRow, columnAtSlot(slot), edge, 0)
}
moveNorth.direction = "north";

function moveSouth(slot) {
	var edge = "";
	var newRow = rowAtSlot(slot) + 1;
	if (newRow > gridwidth - 1) {
		//uh-oh
  	newRow = (0);
    edge = "south";
	}
  return buildResult(newRow, columnAtSlot(slot), edge, 2);
}
moveSouth.direction = "south";

function moveWest(slot) {
	var edge = "";
	var newCol = columnAtSlot(slot) - 1;
  if (newCol < 0) {
  	//uh-oh
    newCol = (gridwidth - 1)
    edge = "west";
  }
  return buildResult(rowAtSlot(slot), newCol, edge, 3);
}
moveWest.direction = "west";

function moveEast(slot) {
	var edge = "";
	var newCol = columnAtSlot(slot) + 1
  if (newCol > gridwidth - 1) {
  	//uh-oh
    newCol = (0)
    edge = "east";
  }
	return buildResult(rowAtSlot(slot), newCol, edge, 1);
}
moveEast.direction = "east"

//TODO: simplify moveUp/moveDown
function moveUp(slot) {
  var climbUp = canClimb(slot, "up")
  var edge = ""
  if (climbUp == true) {
  	edge = "up"
  }
  return buildResult(rowAtSlot(slot), columnAtSlot(slot), edge)
}
moveUp.direction = "up";

function moveDown(slot) {
  var climbDown = canClimb(slot, "down")
  var edge = "";
  if (climbDown == true) {
  	edge = "down"
  }
  return buildResult(rowAtSlot(slot), columnAtSlot(slot), edge)
}
moveDown.direction = "down";

// A destination can appear in either of two forms:
// string "room:N" or
// object {roomName:"room", slot:"N"}
function parseDestination(destination) { // ensure object form of destination
	if (typeof destination === "object")
		return destination;
	if (typeof destination === "string") {
		var pair = destination.split(':');
		return {
			roomName:pair[0],
			slot:pair[1]
		}
	}
	// reject any other type
	return null;
}

function useItemInDirection(directionFunction) {
	useItem(findDestination(directionFunction));
}

function moveDudeInDirection(directionFunction) {
	moveTo(findDestination(directionFunction));
}

function findDestination(directionFunction) {
  // Find a space relative to current location...
	// Returns destination object
	var oldSlot = game.dude.slot;
  var nextRoomName = game.room.name; //assume stay in current room
  var moveResultObj = directionFunction(oldSlot);
	var newSlot = moveResultObj.newSlot;
  var edge = moveResultObj.edge; //"" or word (e.g. "up", "west"

	if (Number.isInteger(moveResultObj.angle)) {
		game.dude.wasFacing = game.dude.facing;
		game.dude.facing = moveResultObj.angle;
	}
  if (edge !== "") { //check for move to new room
  	// crossed an edge into new room...
  	var currentRoomPlan = game.room.plan;
    nextRoomName = currentRoomPlan[edge]; //find neighboring room string
		// string may have format "room" or "room:slot"
  }

	if (!nextRoomName) // no valid room
		return;

	var dest = parseDestination(nextRoomName) // convert from possible string
	if (!dest.slot) // if no slot provided,
		dest.slot = newSlot; // use newSlot

	return dest;
}

function moveTo(target) {
	// target may be string "roomName:slot" or object
	if (!target)
		return;
	target = parseDestination(target);
	// absolute move to slot within target.roomName
  var nextRoom = game.room;  //assume already in room
	if (target.roomName !== game.room.name) {
  	nextRoom = ensureRoom(target.roomName); //nope; prepare other room.
  }
  if (!(target.slot >= 0)) { // if slot not number-like (eg. 0, 1, "1"),
  	target.slot = nextRoom.startSlot; // use default start
  }

  if (canEnter(target.slot, nextRoom)) { // unlocks doors if holding key, or boards vehicles
  	// go ahead with move...
    game.dude.slot = target.slot;
    game.dude.roomName = nextRoom.name;
		if (weather)
			weather.updateLocation();

    consumeLoot(); //every move takes time, triggers decay

    activateRoom(nextRoom);
    displayDude();
    // check for teleporters at destination
    if (maybeTeleport())
      return; // teleported elsewhere; don't do anything else here
    // otherwise...
    //take(slot); // grab anything present
	} else { //can't enter space, just turn toward it
		reorientDude();
	}
	// unless teleported, whether moved or not, try interacting:
	interact(target.slot);
	if (game.dude.status)
		game.dude.status.adjust(1);
}

function maybeTeleport() {
	var slot = game.dude.slot,
			space = game.room.spaces[slot],
			side = entrySide();
	var target = space.teleports;
  if (target  && (typeof target==='string') && directionOK(space.teleportFromSide,side)) {
		moveTo(target);
		return true;
  }
	// else no teleport target found, or entering from wrong direction:
	return false;
}

function directionOK(constraints,side) {
	// constraints: a subset of letters 'news', as in space.entryFrom or space.exitTo
	// side: single-letter of 'news', direction of neighbor space being entered or exited
	if (!constraints) // no restrictions listed
		return true; // allow any action in any direction
	// returns true if side is listed in string constraints
	return (constraints.indexOf(side)>=0);
}
function entrySide() { // --> one letter from 'swne'
	return 'swne'[game.dude.facing];
}
function exitSide() {
	return 'nesw'[game.dude.facing];
}

function canEnterFromSide(space,dude,slot) {
	var side = entrySide();
	var ok = directionOK(space.entryFrom,side);
	if (ok)
		return ok;
	// Even if normal directions don't allow entry, special conditions may allow it anyway...

	var qualifiers = space.qualifiedEntryFrom;
	if (!qualifiers) //no exceptions, give up
		return false;
	var relevantQualifiers = qualifiers[side];
	if (!relevantQualifiers)
		return false;
	// Found possible qualifier...
	for (var i=0; i<relevantQualifiers.length; i++) {
		var conditionFn = relevantQualifiers[i];
		if (conditionFn(slot))
			return true;
		// otherwise continue
	}
	// no items qualified, can't enter
	return false;

}

function canExitToSide(dude) {
	var space = game.room.spaces[dude.slot],
			side = exitSide();
	return directionOK(space.exitTo,side);
}

function canEnter(slotNum, room) {
	var slotObject = room.spaces[slotNum];
  var isBarrier = slotObject.barrier;
	//...
	if (isBarrier==="bridge")
		return true; //allow entry regardless of vehicle
	if (!canExitToSide(game.dude))
		return false;
	if (!canEnterFromSide(slotObject,game.dude,slotNum))
		return false; //no entry from this side, regardless of terrain
	if (isBarrier) {
		if (game.dude.vehicle) { //riding in vehicle; maybe that lets me enter?
			if (game.dude.vehicle.vehicle===isBarrier)
				return true;
		}
		// There is vehicle there, which allows entry despite barrier
		if (slotObject.treasure && slotObject.treasure.vehicle) {
			//boardVehicle(slotObject.treasure);
			return true;
		}
		// Maybe barrier can be opened:
    if (slotObject.locked)
        tryUnlock(slotNum);
  	return false;
  } else {
		// no barrier (assuming walk).  What about if dude is in vehicle?
		if (game.dude.vehicle)
			exitVehicle();
	}
  return true;
}

function canClimb(slotNum, directionStr) {
	//slotNum is what slot we are in. directionStr is if we are
  //going up or down. It is a string reading either "up" or
  //"down".
  var slotObject = game.room.spaces[slotNum];
  var isClimbable = slotObject["climb" + directionStr];
  return isClimbable;
}

function thingIsDeployedThere(slot,thing,direction) {
	// true if a treasure named thing is located in direction from slot and has property 'deployed'
	if (!slot)
		return false;
	var neighbor = direction(slot);
	if (neighbor.edge)
		return false; // off grid edge; can't be adjacenct in this room
	var space = game.room.spaces[neighbor.newSlot];
	return (space.treasure
			&& space.treasure.name===thing
			&& space.treasure.deployed);
}

var moveQualifiers = {
	ropeAbove: function(slot) {
		return thingIsDeployedThere(slot,"rope",moveNorth);
	},
	ladderBelow: function(slot) {
		return thingIsDeployedThere(slot,"ladder",moveSouth);
	}
}
