// Tools for representing, gathering, dropping, and modifying inventory items

// Items are represented in three formats:

// 1) name only: "thing"

// 2) simple object: {name:"thing", properties...}
// These have a constructor of Object, and can be fully represented by JSON.stringify

// 3) class instance: {name:"thing", properties...}
// These have a constructor of function Thing and may inherit properties from Thing.prototype
// JSON.stringify will strip away their constructor, so the name must be included to locate
// its constructor when loading it.
// The constructor is always capitalized, but the name is lowercase; the index ctorIndex
// maps from the name to constructor.

//
// When carried in inventory, items of any type may be stacked if they have no custom properties
// and can be simplified to name-only format.
//
// An inventory record will be one of four formats:
// {thing: N}  --> a stack of N identical simple objects with no properties
// {thing: {name:"thing", properties...}}  --> a single simple object with unique properties
// If there is a constructor Thing:
// {thing: N}  --> a stack of N identical class instances with only inherited properties
// {thing: <Thing>}  --> a single class instance with unique properties


function transferItem() {
  // either get objects on ground or drop selected item
  if (canDropHere(gui.selectedItem)) {
    dropItem(gui.selectedItem.name)
  } else {
    take(game.dude.slot);
  }
  toggleSelection(false);//unselect any items
}


function simplifyItem(item) { // ==> string | object | instance
  // Certain properties (e.g. "deployed") are removed when items are carried.
  // Once those are omitted, if only name remains, item becomes stackable and can be just a string
  delete item.deployed;
  if (item.name && Object.keys(item).length === 1)
		// object with only name property...
    return item.name; // stackable; return string only
  return item; // return full object or string
}

function ensureObject(item) { // ==> object | instance
	//item may be string or object
	if (typeof item==="string") {
		item = {name:item}; // convert string to simple object
	}
	if (typeof item==="object") {
		return rebuildIfInstance(item);
	}
	// not string or object; failure!
	return;
}

function rebuildIfInstance(item) { //==> string | object | instance
	if (typeof item === "object") {
		var ctor = ctorIndex[item.name]; // see if it has a constructor function
		if (typeof ctor==="function") {
			// if so, use constructor to make a class instance
			return new ctor(item);// i.e. replace generic object with correct type
		}
	}
	// didn't rebuild, so it's either a string or simple object:
  return item;
}


function augmentItem(item,change) { // ==> object
	item = ensureObject(item);
	extend(item,change);
	return item;
}

function consumeLoot() {
  // Every turn, certain perishable loot partially decays.
  // This is temporary hack for torches; should be generalized for all objects:
  var torches = game.dude.loot.torchLit;
  if (!torches) return;
  var burnRate = Math.ceil(torches)*.01;
  if (loseItem('torchLit',burnRate)) {
    // fewer torches now, light drops
    displayPersonalLight();
  }
}


function rebuildLoot() {
	// Go through newly-loaded loot and rebuild any generic objects with the appropriate constructor
	var loot = game.dude.loot;
  for (var name in loot) {
    loot[name] = rebuildIfInstance(loot[name]);
	}
}


// Gathering...

function take(slot) {
  // try taking items if any
  var space = game.room.spaces[slot];
  var treasure = space.treasure;
  if (treasure) {
		// 1) remove from world
    modifyRoom(game.room, space, slot, {treasure:false});
    if (treasure.lit) //if item emits light
      displayGrid(game.room); // refresh room lighting
    // redraw space without treasure:
    displayDude(true);

    // 2) place on dude:
    if (treasure.vehicle)
      boardVehicle(treasure);
    else
      gainItem(treasure);
  }
}

function gainItem(item) { // ==> string or object, simplified form of item
  item = simplifyItem(item);
  // item may be either a name or a complete object
  // If it's a name, it has no instance properties and may be stacked
  if (typeof item === "string")
    return gainStackableItem(item);
  else // can't be stacked
    return gainUniqueItem(item);
}

function gainUniqueItem(item) { // ==> item
	// item is object
  var name = item.name;
  if (game.dude.loot[name]) {
		// Already one or more items with this name!
		// TODO: handle this case without failing
    console.log("Already have one of these:", item);
    return false;
  } // otherwise, ok
  game.dude.loot[name] = item;
  displayLootItem(name,1);
  if (item.lit)
    displayPersonalLight();
  return item;
}

function gainStackableItem(name) { // ==> name
  var had = game.dude.loot[name] || 0; //make sure we start with a #
  var nowHave = game.dude.loot[name] = had + 1;
  displayLootItem(name,nowHave);
  return name;
}

function boardVehicle(vehicleObj) {
  game.dude.vehicle = vehicleObj;
  displayDude();
}


// Releasing...

function canDropHere(selectedItemName) { // ==> item name | false
  var here = spaceHere();
  if (here.name === "water") //over water
    return false;
  if (here.treasure) // already something here
    return false;
  else
		return selectedItemName; //may still be false
}

function exitVehicle() {
  var vehicle = game.dude.vehicle;
  delete game.dude.vehicle;
  // redecorate dude's icon to exclude vehicle:
  var icon = document.getElementById('dude');
  icon.classList.remove(vehicle.name);
  // Leave vehicle here as item:
  vehicle.facing = game.dude.facing;
  placeItem(vehicle);
}

function whicheverIsComplex(one,two) { // ==> object when possible, else string
	if (typeof one==="string")
		return two || one;
	if (typeof two=="string")
		return one || two;
	if (Object.keys(one).length>Object.keys(two).length)
		return one;
	return two;
}

function dropItem(requestedItem) {
	// requestedItem is string or object
	// Find and remove the inventory item which best matches:
	var hadItem = loseItem(requestedItem.name || requestedItem);
  if (hadItem) {
		// Place on ground whichever item has more detailed information:
    placeItem(whicheverIsComplex(requestedItem,hadItem));
    toggleSelection(); // unselect everything
  }
}

function placeItem(item) {
	// item is string or object
  // Does not remove from inventory, but adds a new item on the ground
  var room = game.room,
      slot = game.dude.slot,
      space = game.room.spaces[slot];
  modifyRoom(game.room,space,slot,{treasure:item});
  if (item.lit)
    displayGrid(room); //change room lighting //TODO: modify darkness w/o complete redraw
  displayTreasure(item,slot); //redraw space with item
  displayDude(); // superimpose dude
}

function loseItem(name,amount) { // ==> string | object | false | undef
  // Amount (default 1) may be fractional.
  // If dude has at least amount of name, lose it.
  // If fewer whole units remain, return the item or name;
  // otherwise return false
  if (typeof amount === 'undefined') {
    amount = 1;
  }
  var had, remaining,
			item = game.dude.loot[name]; //a # if stacked, otherwise a unique object
  if (typeof item === "object") { // unstacked/unique item
    had = 1;
	} else if (typeof item === "number") { // stacked items
		had = item;
		item = null;
	} else //invalid format, can't deal with this
		return;

  if (amount<1 || had >= amount) {
    remaining = had - amount;

    if (remaining<=0) { // If nothing remains...
      remaining=0;
      delete game.dude.loot[name]; // remove completely from inventory
    } else {
      game.dude.loot[name]=remaining;
    }
    displayLootItem(name,remaining);

    var fewerUnitsNow = Math.ceil(remaining)<Math.ceil(had);
		if (fewerUnitsNow) { // consumed last of a unit of that type
			return item || name;  //return consumed object (if unique) or name (if stackable)
		}
  }
  return false; // same number of whole units remain
}
