// Copyright A and D 2020

/* ----- Lighting version 1 -----
Calculate distance from each space to each light source,
accumulate illumination in room.light array,
and let each space handle its own rendering.

function spacesAffected(originSlot,magnitude) {
	var result = {
				slots:[],
				distances:[]
			},
			radius = Math.floor(magnitude * 8),
			originY = rowAtSlot(originSlot),
			originX = columnAtSlot(originSlot);
	for (var dy=-radius; dy<= radius; dy++) {
		for (var dx=-radius; dx<= radius; dx++) {
			var x = originX+dx,
					y = originY+dy;
			if (x>=0 && y>=0 && x<gridwidth && y<gridwidth) {
				var distance = Math.sqrt(dx*dx+dy*dy);
				if (distance<=radius) {
					result.slots.push(SlotAtRowCol(y,x))
					result.distances.push(distance);
				}
			}
		}
	}
	return result;
}

function maybeRadiate(room,slot,space) {
	if (space.glows) {
		var areaOfEffect = spacesAffected(slot,space.glows);
		for (var which=0; which<areaOfEffect.slots.length; which++) {
			var nearby = areaOfEffect.slots[which],
					distance = areaOfEffect.distances[which],
					currentLight = room.lighting[nearby] || 0;
			if (distance<.5)
				distance = .5; //eliminate zeros
			currentLight += space.glows/distance;///(distance*distance);
			room.lighting[nearby] = currentLight;
		}
	}
}
*/

/* ----- Version 2 -----
Use darkness layer over entire map, normally black, and punch holes in it
with transparency gradients for each light source.

*/
function toggleDaylight(box) {
	var visible = box.classList.contains('nightTheme')? "initial":"hidden";
	darkness.style.visibility = visible;
}

var darkness = null;
function makeDarkness() {
	if (darkness) {
		var hidden = darkness.style.visibility;
		darkness.remove();
	}
	darkness = document.createElement('div');
	darkness.id = 'darkness';
	darkness.lights = [];   // for fixed terrain
	darkness.lights.compositions = [];
	darkness.personal = null;  // may be created by displayPersonalLight()
	darkness.style.visibility = hidden;

	darkness.addEventListener("click",clickOnMap,false);
}

function renderDarkness(portal,room) {
	// copy all light sources into mask
	var lights = darkness.lights.slice();
			compositions = darkness.lights.compositions.slice();
	if (darkness.personal) {
		lights = lights.concat(darkness.personal);
		compositions = compositions.concat(darkness.personal.compositions)
	}
	// Last/topmost layer is always full black:
	lights.push('linear-gradient(black,black)');
	compositions.push('source-out');

	var mask = lights.join(',');

	darkness.style['-webkit-mask-composite'] = compositions.join(',');
	darkness.style['-webkit-mask'] = mask;
	// Non-webkit browsers (e.g. Firefox) use mask property instead, with only "intersect" composition
	darkness.style['mask'] = mask;
	darkness.style['mask-composite'] = 'intersect';
	// Ambient light:
	if (room) {
		var daylight = room.plan.daylight || 0; // room is dark unless daylight specified
		darkness.style.opacity = 1-daylight;
	}
	if (portal)
		portal.appendChild(darkness);

}


// Every light source has potentially a glow (radial) and beam (directed) component
// glow: brightness of radial halo (0..1)
// beam: brightness of beam (0..1)
// angle: angle of beam (0..360).  If omitted, assume player's forward direction.
// width: half-angle of beam width (0..90)

function addLightSource(slot,light,list) {
	// slot: position of source within room
	// light: either scaler (0..1) or object {glow, beam...}
	// list (optional): a set of sources where light will be added

	if (!list)
		list = darkness.lights; // use fixed sources by default

	if (typeof light !== 'object') {	// if light is scalar,
		light = {glow:light};						// convert to object
	}

	// light may have both glow and beam
	var glow = light.glow || 0,
			beam = light.beam || 0;
	if (!glow && !beam) return; // nothing to do

	var xoffset = light.xoffset || 0,
			yoffset = light.yoffset || 0,
			cx = pixelX(slot,xoffset),
			cy = pixelY(slot,yoffset);

	if (glow)
		addLightGlow(list,cx,cy,glow);
	if (beam)
		addLightBeam(list,cx,cy,beam,light);
}

function addLightGlow(list,cx,cy,glow) {
	// cx, cy: center of halo
	// glow: brightness (0..1) of halo
	var transmission = 70,
			// transmission: arbitrary scaling constant, converts magnitude into range
			range = (glow*transmission)+'%';
			// range: the radius of a glow=1 aura, beyond which is full darkness,
			//  as a percentage of grid with
	list.push(
		`radial-gradient(circle at ${cx}px ${cy}px, rgba(0,0,0,${1-glow}), black ${range})`
	);
	list.compositions.push('source-in');//intersect transparent aura with prior mask
}

function addLightBeam(list,cx,cy,beam,light) {
	// cx, cy: origin of beam
	// beam: brightness (0..1) of light ray
	var width = light.width || 35, // in degrees
			// width: half-angle of beam spread
			angle = Number.isInteger(light.angle)? light.angle: game.dude.facing*90,
			// angle: direction of beam
		mask, //string for this mask layer
		operator; //composition with next layer

	if (CSS.supports('background','conic-gradient(black,white)')) {
		// if conic-gradients work...
		mask = `conic-gradient(from ${angle}deg at ${cx}px ${cy}px, `+
			`rgba(0,0,0,${1-beam}), black ${width}deg, `+					// right half from center to darkness
			`black ${360-width}deg, rgba(0,0,0,${1-beam}) 1turn)`;// left half from darkness to center
		operator = 'source-in'; //intersect this transparent beam with prior mask, adding transparency
	} else {
		// Conic-gradients still don't work on Safari.  Here's another solution:
		// create an SVG with an inverted gradient (black opaque center) clipped into a triangle,
		// then use subtraction composition operator (destination-out)
		width = width/250; //convert to (roughly) pixels
		mask = `url('data:image/svg+xml;utf8,`+
		`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 480 480" preserveAspectRatio="none">`+
			`<defs><linearGradient id="beamGradient" gradientUnits="objectBoundingBox" x2="1" y2="0">`+
				`<stop stop-opacity="0" offset="${.5-width}"/>`+ //transparent
				`<stop stop-opacity="${beam}" offset=".5"/>`+    //opaque
				`<stop stop-opacity="0" offset="${.5+width}"/>`+ //transparent
			`</linearGradient></defs>`+
			// triangular clipping area:
			`<polygon id="beamCone" points="-250,-500 0,0 250,-500"`+ //fixed angle, big enough to cross full grid
				`fill="url(#beamGradient)"`+ //filled with gradient above
				`transform="translate(${cx},${cy}) rotate(${angle})"/>`+ //moved to beam source and rotation
		`</svg>')`;
		operator = 'destination-out'; //subtract this opaque beam from prior mask, adding transparency
	}

	list.push(mask);
	list.compositions.push(operator);
}

function displayPersonalLight() {
	var dude = game.dude;
	darkness.personal = []; // clear existing personal lights, rebuilding...
	darkness.personal.compositions = [];

	var source;
	if (source=dude.loot.flashlightLit) {
		addLightSource(dude.slot, source.light, darkness.personal);
	}
	if (source=dude.loot.lanternLit) {
		addLightSource(dude.slot, source.light, darkness.personal);
	}
	// TODO: get lighting parameters from treasures database instead of hard-coding here
	if (source=dude.loot.torchLit) {
		var light = Math.ceil(source)*.5; // .5 per torch remaining
		addLightSource(dude.slot, light, darkness.personal);
	}

	renderDarkness();
}
