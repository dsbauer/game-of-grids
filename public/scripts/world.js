//Copyright 2019-2020 by A and D
/**** Making & updating game world *****/

function RandomVariable(max) { // random integer from 0 up to but excluding max
 return Math.floor(Math.random() * (max));
}

function convertMinimap(plan) {
  // A room plan's minimap may include inline details, objects following a map line
  // which customize certain spaces on that line.
  // The function strips out those objects, remembering which spaces they refer to,
  // and places them all together in room.plan.details.
  // The minimap is then assembled from the strings starting each line.

  var list = plan.minimap, //currently a list of strings with some embedded customizing objects
      strings = [],  //will store 8 strings of length 8
      slotOffset = 0, // total length of strings (map rows) preceding current line
      lastLength = 0; // length of the string on previous line

  function stashDetail(item) {
    // item should have form {N:{details}, M:{details}...}
    //  where N,M are 0..7, referring to spaces in this row (i.e. relative slots)
    //  and each details object has 1+ properties which customize that space.
    // Go through each entry...
    for (var key in item) {
      var n = Number.parseInt(key),
          spaceDetail = item[key];
      if (!(n>=0)) //make sure key is a number
        console.log("Map detail has non-numeric index:",n);
      if (n>=gridwidth)
        console.log("Map detail index exceeds map width:",n);
      // stash details in plan.details index
      if (!plan.details) // make it if needed
        plan.details = {};

      plan.details[slotOffset+n] = spaceDetail;
    }
  }

  for (var i=0; i<list.length; ++i) {// for every list item...
    var item = list[i];
    if (typeof item==='string') {
      //starting new string; change offset by length of last string
      slotOffset += lastLength;
      lastLength = item.length;  //should be gridwidth
      if (item.length!==gridwidth)
        console.log('Minimap row has wrong length:', item);
      strings.push(item); // collect this string
    } else if (typeof item==='object') {
      // inline details found...
      stashDetail(item);
    } else {
      console.log('Invalid map detail:', item);
    }
  }
  // Now all details have been stashed and strings collected;
  // join the strings together

  if (strings.length!==gridwidth)
    console.log('Wrong number of minimap rows:', strings.length);
  plan.minimap = strings.join('');
}

function groundFromMap(room, slot) {
 var letters = room.plan.minimap;
 var letter = letters[slot];
 if (letter == "*") {
	 room.startSlot = slot; //remember it as starting space,
	 letter = ".";    // then it becomes just another empty space
 }
 var groundObj = groundtypes[letter];
 return groundObj;
}

//TODO: function treasureFromMap(room,slot) {}
function treasureFromMap(room, slot) {
  var letters = room.plan.treasuremap;
  if (letters) {
    var letter = letters[slot];
    var treasureObj = treasuretypes[letter];
    if (typeof treasureObj === 'function')
      return new treasureObj(); //build item using constructor function
    else
      return treasureObj;
  }
}

function makeSpace(room, slot) {
  var groundObj = groundFromMap(room,slot);  //groundObj is an object representing the ground type.
  var treasureObj = treasureFromMap(room,slot);
  var space = duplicateGround(room, groundObj);

  if (treasureObj) {
   space.treasure = treasureObj;
  }

  // maybe modify space according to plan.details
  var localDetails = room.plan.details && room.plan.details[slot];
  if (localDetails)
    extend(space,localDetails);

 // adjust default space using accumulated changes:
 var roomChanges = game.changes[room.name],
     spaceChanges = roomChanges && roomChanges[slot];
 if (spaceChanges)
   extend(space,spaceChanges);

  if (space.treasure)
    space.treasure = rebuildIfInstance(space.treasure);
  // now that space is customized, check whether it's a light or sound emitter
  // and add its effect to lighting grid
  //maybeRadiate(room,slot,space);

 return space;
}

function duplicateGround(room, ground) {
 // Make a copy so we can customize it:
 var duplicate = Object.assign({},ground);
 // If the ground type is a teleporter, attach its target:
 //if (duplicate.teleports && room.targets) {
//	 duplicate.teleports = room.targets.shift();
 //}
 return duplicate;
}

function ensureRoom(roomName) {
 //either retrieve correct room if already built,
 // or build it the first time.
 var room = game.visited[roomName];
 if (!room) {
	 room = game.visited[roomName] = makeRoom(roomName);
 }
 return room;
}

function makeRoom(roomName) {
 //construct one room from its map, filling in room.spaces
 var room = {};
 room.name = roomName;
 room.plan = RoomPlans[roomName]; // the object describing the room's spaces and exits
 convertMinimap(room.plan); // removes inline details from minimap, places into room.plan.details

 //room.lighting = [];
 //room.lighting.ambient = room.plan.daylight || .6;
 room.spaces = []; //<--building this...
 //if (room.plan.teleportTargets) {
//	 room.targets = room.plan.teleportTargets.slice();//copy teleportTargets list
 //}
 var slot = 0;
 while (slot < gridwidth * gridwidth)
 {
	 room.spaces[slot] = makeSpace(room,slot);
	 slot = slot + 1;
 }
 // ensure a startSlot if none:
 if (typeof room.startSlot !== "number") {
	 room.startSlot = Math.floor((gridwidth+1)*gridwidth/2);//center
 }
 return room;
}

function extend(object,changes) {
  // Simple version of jQuery's extend()...
  // Mix changes into object
  object = object || {};
  for (var property in changes) {
    object[property] = changes[property];
  }
  return object;
}

function spaceHere() {
  return game.room.spaces[game.dude.slot]
}

function modifyRoom(room, space, slot, change) {
  // mix change into the cumulative record for this room:
  var roomChanges = game.changes[room.name] || {};
  roomChanges[slot] = extend(roomChanges[slot], change);
  game.changes[room.name] = roomChanges;

  // before applying changes to space,
  // use old values to disable certain display classes:
  undecorateSpace(space, slot, change);
  // apply change to space itself:
  extend(space,change);
}

function makeDude(room,skin) {
  //create a new character
  game.dude = {
    skin: skin,
    roomName: room.name,
    slot: room.startSlot,
    facing: 0,
    loot: {}
  }
  if (typeof Status==="function") //if Status module included...
    game.dude.status = new Status();
}

function tryUnlock(slot) {
 // For now, bypass transformGround(); modify object instead of replacing it
 var newDoor = groundtypes["d"];
 // transformGround(slot, newDoor);
 var oldDoor = game.room.spaces[slot];
 var color = oldDoor.locked;
 var keyname = color && color+"-key";
 var canUnlock = keyname && loseItem(keyname);
 if (!canUnlock)
   return false;
 // disable the door...
 modifyRoom(game.room, oldDoor, slot, {
   name:"openDoor",
   locked:newDoor.locked || false,
   barrier:newDoor.barrier || false
 });

 redisplaySpace(slot, oldDoor);
 return true;
}

function interact(slot) {
  // dude interacts with slots by walking into them
  // or bumping them (if barrier)
  var space = game.room.spaces[slot];
  var status = space.status;
  if (status==='quenching' && !game.dude.loot.umbrella) { // wet places
    if (game.dude.status) {
      game.dude.status.damp = 100;
      game.dude.status.adjust();
    }
    // extinguish any carried torches and lanterns
    if (game.dude.loot.torchLit || game.dude.loot.lanternLit) {
      var extinguished=0;
      if (loseItem('torchLit',game.dude.loot.torchLit))
        extinguished++;
      if (loseItem('lanternLit',1)) {
        extinguished++;
        gainItem('lantern'); //keep unlit lantern
      }
      if (extinguished>0)
        displayPersonalLight();
    }
  }
}

function pairsMatch(nameA,nameB,valA,valB) {
  return (nameA===valA && nameB===valB) ||
      (nameA===valB && nameB===valA);
}
function useItemWithItem(targetName) {
  var subject = gui.selectedItem;
  if (!subject || !targetName)
    return;
  var subjectName = subject.name;
  if (pairsMatch(subjectName,targetName,"torchLit","stick")) {
    igniteStick();
  } else if (pairsMatch(subjectName,targetName,"torchLit","lantern")) {
    igniteLantern();
  } else if (pairsMatch(subjectName,targetName,"battery","flashlight")) {
    // light flashlight with batteries
    if (loseItem('battery') && loseItem('flashlight')) {
      gainItem(ensureObject("flashlightLit"));
      displayDude();
      toggleSelection();
    }
  }
}

function igniteStick() {
  if (game.dude.status.dryEnough(30) &&
    loseItem('stick')) { //if dude has a stick to lose
    gainItem('torchLit');// gain a lit torch instead
    displayDude();
    toggleSelection();
  }
}
function igniteLantern() {
  if (game.dude.status.dryEnough(70) &&
    loseItem('lantern')) {
    gainItem(ensureObject("lanternLit"));
    displayDude();
    toggleSelection();
  }
}

function useItem(destination) {
	var item = gui.selectedItem;
	if (!item || !destination)
		return;
	var itemName = item.name;
  var slot = destination.slot;
	var space = game.room.spaces[slot];
	var status = space.status;

  // Temporarily disable: interferes with deploying ladders up cliffs
  //if (!(canEnterFromSide(space,game.dude)))
  //  return;

  if (itemName==='flashlightLit') {
    displayPersonalLight(); //reorient flashlight beam
  }

  if (itemName==='stick' && status==='burning') {
    // space is burning, can ignite carried items (e.g. stick)
    igniteStick();
  } else if (itemName==='lantern' && status==='burning') {
    // ignite lantern from adjacent fire
    if (game.dude.status &&
        game.dude.status.canFocus(30,`lighting the lantern`))
      igniteLantern();
  } else if (itemName==='torchLit' && status==='flammable') {
    // light adjacent space with carried torch
    modifyRoom(game.room, space, slot, {status:'burning', lit:true, climbup:false});
    redisplaySpace(slot, space);
    displayGrid(game.room);//ensure correct lighting
    toggleSelection();
  } else if (itemName==='pickaxe' && space.diggable) {
    // dig rubble
    modifyRoom(game.room, space, slot, {name:"", barrier:false, diggable:false});
    redisplaySpace(slot,space);
  } else if (itemName==='coin' && status==='vending') {
    // purchase from vending machine
    if (loseItem('coin',space.price)) {
      modifyRoom(game.room,space,slot,{status:'used'});
      gainItem(space.loot);
      redisplaySpace(slot,space);
      displayDude();
      toggleSelection();
    }
  } else if (space.climbable) {
    // deploy a climbing rope or ladder
    if ((itemName==='rope' && game.dude.facing===2 && spaceHere().name==='bush') ||
        (itemName==='ladder' && game.dude.facing===0 && spaceHere().name!=='water')) {
      if (game.dude.status && game.dude.status.canFocus(40,`setting up the ${itemName}`))
  		  dropItem({name:itemName, deployed:true});
    }
	} else {
		console.log(`using ${itemName} in slot ${slot}:`,space);
  }
}


/* ---- Loading and Saving ------
The game state is maintained in two objects:
* game.dude tracks the player's location and inventory
* game.changes tracks every change made to the default world (as encoded by maps)

These two objects are encoded in window.localStorage as JSON strings.

The full details of each room (game.rooms) can be rebuilt from the maps + game.changes
and don't need to be saved.
*/

function getSavedGame() {
  var json = window.localStorage.getItem('game_of_grids_save'),
      gameState = json && JSON.parse(json);
  if (gameState && gameState.dude && gameState.changes)
    return gameState;
  return null; //
}

var weather = null;
function saveWorld() {
  var gameState = {
    dude: game.dude,
    changes: game.changes,
    weather: weather && weather.export()
  };
  var json = JSON.stringify(gameState);
  try {
    window.localStorage.setItem('game_of_grids_save',json);
  } catch {
    console.log("Game cannot be saved!")
  }
  return json;
}

function loadWorld() {
  var gameState = getSavedGame();
  if (gameState) {
    game.dude = gameState.dude;
    rebuildLoot();
    game.changes = gameState.changes;
    if (game.dude.status) {
      game.dude.status = new Status(game.dude.status);
    }
    if (gameState.weather && typeof weatherModule!=='undefined')
      weatherModule.transitionWeather(
        gameState.weather.type,  // rain/fog/snow
        gameState.weather.level, // 0..1
        0); // 0msec, instantly
  }
  return gameState;
}
