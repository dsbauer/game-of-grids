// Copyright A and D 2020
//var song;

//functon preLoadMusic() {
 // song = loadSound("perplexingpool.mp3");
//}

//function musicSetup() {
  //song.play();
//}


function startAmbience(room) {
  var label = room.plan && room.plan.sound;
  updateSound(label);
}

function soundPanel() { //panel is outer element
  return document.getElementById('ambientSoundPanel');
}
// panel components:
function soundPlayer() {
  return document.getElementById('ambientSoundPlayer');
}
function audioElement() {
  return document.getElementById('ambientSoundElement');
}
function soundCaption() {
  return document.getElementById('ambientSoundCaption');
}

function toggleSound(mute) {
  var audio = audioElement(),
      panel = soundPanel();
  if (arguments.length < 1) {// if mute not specified...
    mute = !currentSound.muted; // switch to opposite
  }
  currentSound.muted = mute;
  // show status on panel:
  if (mute) {
    panel.className='muted'
  } else {
    panel.className='';
  }
  if (audio) { // if audio element exists...
    audio.muted = mute; // turn it on or off
  }
}

var currentSound = {}

function makeAudio(sound) {
  // Build the <audio> element for the given sound object
  var audio = document.createElement('audio');
  audio.id = "ambientSoundElement";
  audio.autoplay = true;
  audio.loop = true;
  audio.volume = sound.volume || 1;
  audio.muted = currentSound.muted;
  audio.src = sound.src;
  return audio;
}

function updateSound(label) {
  var player = soundPlayer(),
      caption = soundCaption();
  if (label === currentSound.label) //same sound as before...
    return; // do nothing

  // sound has changed (maybe to silence); always clear current elements
  currentSound.label = label;// set current label
  player.innerHTML = ''; // destroy audio element
  caption.innerHTML = '';// destroy caption

  if (label) { // install new sound
    var sound = soundLibrary[label], // get corresponding sound object from library
        audio = makeAudio(sound); // make audio element
    player.appendChild(audio); // attach it to player

    toggleSound(currentSound.muted); // set toggle button to show muted status

    if (sound.license==="attribution") { // display new attribution, if any
      var title = sound.title? `"${sound.title}"` : 'untitled';
      caption.innerHTML =
        `<a href='${sound.origin}' target="_blank">${title} by ${sound.author}</a>`;
    }
  }
}

const soundLibrary = {
  forest: {
    origin:"https://freesound.org/s/506103/",
    license:"attribution",
    title:"Forest",
    author:"JayHu",
    src:"https://freesound.org/data/previews/506/506103_5917175-lq.mp3"
  },
  ocean: {
    origin:"https://freesound.org/s/465282/",
    license:"public",
    volume:0.3,
    src:"https://freesound.org/data/previews/465/465282_7311349-lq.mp3"
  },
  water: {
    origin:"https://freesound.org/people/deleted_user_229898/sounds/150179/",
    license:"public",
    src:"https://freesound.org/data/previews/150/150179_229898-lq.mp3"
  },
  waterfall: {
    origin:"https://freesound.org/s/110625/",
    license:"attribution",
    title: "Waterfall",
    author:"soundscalpel.com",
    src:"https://freesound.org/data/previews/110/110625_1930766-lq.mp3"
  },
  dungeon: {
    origin:"https://freesound.org/people/Kinoton/sounds/516566/",
    license:"public",
    src:"https://freesound.org/data/previews/516/516566_2247456-lq.mp3"
  },
  cave0: {
    origin:"https://freesound.org/s/443061/",
    license:"public",
    src:"https://freesound.org/data/previews/443/443061_6229882-lq.mp3"
  },
  cave1: {
    origin:"https://freesound.org/s/177958/",
    license:"public",
    src:"https://freesound.org/data/previews/177/177958_985466-lq.mp3"
  },
  cave2: {
    origin:"https://freesound.org/s/270387/",
    license:"attribution",
    author:"LittleRobotSoundFactory",
    src:"https://freesound.org/data/previews/270/270387_5123851-lq.mp3"
  }

}