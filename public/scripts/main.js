//Copyright 2019-2020 by A and D
// Global object holding all game-state properties:
var game = {
	room: { // the current room object
  //-- These values will be replaced automatically: --
  	name: "",
  	plan: {},   //one of the room objects from RoomPlans
  	spaces: [], //an array of space objects (from groundtypes) for the current room
		//changes: {}, //a subset of spaces with corresponding changes to original plan
    startSlot: 0 // where dude first appears in that room
  },
  visited: {}, // a collection of all rooms visited so far, including game.room
  dude: { // an object representing the player character
		roomName: "", //
    slot: 0, //current location within room
    loot: {}, //count of treasures found, by treasure name
    skin: "girlA",
		facing: 0 // integer 0..3 (N,E,S,W) representing direction dude is facing
  },
	changes:{}
};

/******** User input ********/
function prepInput() {
	 document.addEventListener("keydown",handleKeypress,false);
	 bindControlButton('btnW','w','ArrowUp');
	 bindControlButton('btnS','s','ArrowDown');
	 bindControlButton('btnA','a','ArrowLeft');
	 bindControlButton('btnD','d','ArrowRight');
	 bindControlButton('btnQ','q');
	 bindControlButton('btnE','e');
	 bindControlButton('transferButton','g');
}

function bindControlButton(id,key,arrowKey) {
	var button = document.getElementById(id);
	// WASD buttons should also flash when arrow keys are used,
	// so those buttons need to be findable by arrowKey as well as WASD key
	if (arrowKey) {
		button.classList.add('button-'+arrowKey);
	}
	button.addEventListener("click",function(){
		var fakeEvent = {key:key};
		handleKeypress(fakeEvent);// simulate keypress with fake event
	},false);
}

function flashButton(key) {
	var buttonID = 'button-'+key,
			buttons = document.getElementsByClassName(buttonID),
			button = buttons[0];
	if (button) { //found the correct button...
		button.classList.add('pressed');// highlight it now, and
		window.setTimeout(function(){  	// schedule it to unhighlight in 150ms
			button.classList.remove('pressed')
		}, 150)
	}
}

function clickOnMap(event) {
	// clicked on map; find relative direction from dude
	// and simulate the corresponding keypress
	var key,
			dudeC = columnAtSlot(game.dude.slot),
			dudeR = rowAtSlot(game.dude.slot),
			// If dude is on edge of map, bias click in that direction
			// to create more space for click/touch
			bias = .3;
	if (dudeC===0)
		dudeC += bias;
	else if (dudeC===gridwidth-1)
		dudeC -= bias;
	if (dudeR===0)
		dudeR += bias;
	else if (dudeR===gridwidth-1)
		dudeR -= bias;
	var dudeX = (dudeC+.5)*spaceWidth,
			dudeY = (dudeR+.5)*spaceWidth,
			clickX = event.offsetX,
			clickY = event.offsetY,
			dx = clickX-dudeX,
			dy = clickY-dudeY;
	if (Math.abs(dx)>=Math.abs(dy)) { //horizontal
		key = (Math.sign(dx)>0)? 'd':'a';
	} else { //vertical
		key = (Math.sign(dy)>0)? 's':'w';
	}
	handleKeypress({key:key});
}

function handleKeypress(event) {
	// This could be called either by an actual keypress or by clicking the corresponding button
	if (event.preventDefault)
		event.preventDefault();
  var actions = {
  	ArrowUp: moveNorth,
    ArrowDown: moveSouth,
    ArrowLeft: moveWest,
    ArrowRight: moveEast,
    w: moveNorth,
    s: moveSouth,
    a: moveWest,
    d: moveEast,
    q: moveUp,
    e: moveDown,
		g: transferItem
  }

	flashButton(event.key); //Briefly highlight corresponding button, even if key pressed

  var action = actions[event.key];
  if (typeof action == "function") {
		if (action.direction) {
			if (gui.selectedItem)
				useItemInDirection(action);
			else
  			moveDudeInDirection(action);
		} else //non-directional, e.g. get/drop
			action();
		// Autosave after every move:
		saveWorld();
  }
}

function toggleSidebar() {
	var sidebar = document.getElementById('sidebar'),
			toggleButton = document.getElementById('toggleSidebarButton'),
			status = (sidebar.classList.toggle('hideControls'));
	toggleButton.innerHTML = status? "Show controls" : "Hide controls";
}
//end of user input

function activateRoom(room) {
	if (room!==game.room) {
		game.room = room;
 	 	displayGrid(game.room);
  }
}

function prepareGame(room) {
	prepInput();
	displayAllLoot();
	activateRoom(room);
}

function prepareInterface() {
	preparePortal();
	prepareSidebar();
	// Include weather controls for now, for developers' use.
	// Eventually they'll disappear for players, with weather managed by game locations
	if (typeof weatherModule!=='undefined')
		weatherModule.makeWeatherControls();
	if (typeof Status==="function")
		Status.prepare();
}

function startGame(skin) {
	prepareInterface();
	var room = ensureRoom(startingRoomName);
	makeDude(room,skin); // place dude at room's start marker *,
	prepareGame(room);
}

function resumeGame() {
	prepareInterface();
	loadWorld();
	var room = ensureRoom(game.dude.roomName);
	prepareGame(room);
}

makeStartButton("boyA");
makeStartButton("girlA");
makeStartButton("corgi");
makeLoadButton();

function makeLoadButton() {
	if (getSavedGame()) { // if saved game exists...
		var portal = document.getElementById('portal'),
				button = document.createElement('button');
		button.innerText = 'Continue';
		button.className = 'loadButton';
		button.onclick = resumeGame;

		var message = document.createElement('p');
		message.innerHTML = 'Or continue your current game:';

		portal.appendChild(message);
		portal.appendChild(button);
	}
}

function makeStartButton(skin) {
  var portal = document.getElementById('portal');
  var button = document.createElement('button');
  button.className = 'startButton '+skin;
  button.onclick = function() { startGame(skin) }
  portal.appendChild(button);
}
