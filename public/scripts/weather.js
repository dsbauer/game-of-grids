/* Copyright 2020 by D
 * This whole file is optional; it can be missing from project
 * and game will run normally but without weather.
 */

const weatherModule = (function(){ //module is wrapped in a function to keep components private

function lockTransitions(bool) {
  var controls = document.getElementById('weatherControls');
  if (controls)
    controls.classList.toggle('locked',bool)
  transitionLock = bool;
}

var transitionLock = false;

// Primary public method:
function transitionWeather(type,level,time) {
	// does the same as changeWeather, but slowly.
  // time is msec per full transition (0 to 100%).
  // Default time is 10000 (10sec)

  if (transitionLock)
    return; //another transition is already running; abort

	if (type==='clear')
		level=0;

	function finish(){
    lockTransitions(false);
	}

  lockTransitions(true);
  displayWeatherTarget(type,level);

  if (typeof time !== 'number')
    time = 10000;
  if (!time) {
    finish();
    return changeWeather(type,level);
  }

  var tick = clamp(time/50, 100, 1000); //default 200, but allows 100...1000
  var step = tick/time; //default .02; incremental change to weather level each tick

	function descend(target,ascend) {
		var current = weather.level;
		var timer = window.setInterval(function() {
			current-=step;
			if (current<=target)
				current=target;
			changeWeather(current);
			if (current<=target) {
				window.clearInterval(timer);
				if (ascend)
					ascend(0);
				else
					finish();
			}
		}, tick);
	}
	function ascend(current) {
		var timer = window.setInterval(function() {
			current+=step;
			if (current>level)
				current=level;
			changeWeather(type, current);
			if (current>=level) {
				window.clearInterval(timer);
				finish();
			}
		}, tick);
	}

	if (!weather)
		ascend(0);
	else if (weather.type !== type) //different type:
		descend(0,ascend); //descend first, then ascend
	else if (level < weather.level)
		descend(level); //descend to level
	else
		ascend(weather.level); //ascend to level
}

function changeWeather(arg0,arg1) {
	// Instantly start, end, or change global weather
	// type (string): if included, sets weather type
	// lvl (number): if included, sets level of type (if included) or current type (if omitted)
	// Valid input patterns:
  //   (type, lvl)
  //   (lvl, type)
  //   (type): set to type at full (level 1)
  // If current weather:
  //   (lvl): change current weather to lvl
  //   (): stop current weather

	var type = (typeof arg0==='string')? arg0:
						 (typeof arg1==='string')? arg1: undefined;
	var lvl = (typeof arg0 ==='number')? arg0:
						(typeof arg1 ==='number')? arg1: undefined;

	if (lvl<=0 || (!lvl && !type)) { //stop current weather, if any
			weather && weather.adjustLevel(0);
			pauseWeather();
			weather = null;
			displayWeatherStatus('',0);
			return true; // ok
	}
	// ensure valid lvl:
	if (!lvl || lvl>1) lvl = 1;
	// ensure valid type:
	if (!type) {
		if (weather)
			type = weather.type;
		else
			return false; //no current weather and no type specified
	}

	// Now we have both a type and a non-zero lvl; make it happen
	if (!weather) { // no current weather...
		var Type = Weather.types[type]; //find corresponding constructor
		if (!Type) return false; //no such type
		weather = new Type(lvl);
		return weather.resume();
	}
	if (weather.type===type) { //same type as current...
		weather.level = lvl;
		return weather.resume();
	}
	// else current weather is different type
	// for now...
	return false;
}

function parseRequest(str) {
  var pair = str.split(':');
  return {type: pair[0], level: 1*pair[1], time:1*pair[2]};
}

function resumeWeather(request) {
  if (typeof request === "string") {
    var result = parseRequest(request);
    transitionWeather(result.type,result.level,result.time);
    return;
  }
	return weather && weather.resume();
}

function pauseWeather() {
	// called when weather is suspended locally (e.g. indoors)
	return weather && weather.pause();
}
/*---- end public interface ----*/

function makeWeatherLayer(id) {
	var weather = document.createElement('div');
	weather.className = 'weather';
	weather.id = id;
	return weather;
}

// Weather types are subclasses of base class Weather:
class Weather {
	constructor(level) {
		this.level = level;
	}
	get type() {
		return this.constructor.name.toLowerCase();//'fog', 'rain', or 'snow'
	}

	export() {
		return {
			type:this.type,
			level:this.level
		}
	}

	pause() {
		// stop mechanism but retain description
		if (this.div) {
			stopAnimating(this.div);
			this.div.remove();
			delete this.div;
			return true;
		}
	}
	resume() {
		displayWeatherStatus(this.type,this.level);
		// restore mechanism from description
		if (!game.room.plan.weather) return false; //not allowed here
		if (this.div) {
			// currently playing (though maybe detached), just reattach into DOM
      grid.after(this.div);
			//portal.insertBefore(this.div,darkness);
		} else {
			// not currently playing (e.g. indoors); rebuild and reanimate
			this.makeDiv();
		}

		this.adjustLevel(this.level);

		if (!(weather.div.timer >= 0)) // if not aready animated
			weather.animate();
		return true;
	}

  updateLocation() {} //override this function if we need to know when player moves
}

function percentString(fraction,str) {
  var num = Math.floor(fraction*100);
  return str? num+str : num;
}

/* Fog behavior:
A semi-transparent fog layer has an eye, a spot of greater transparency, which constantly
moves toward player.
Version 1 (currently active): eye jumps to player after every move, no further animation.
Version 2:  Animated eye moves partway toward player with every clock tick, regardless of player moves.
After moving quickly, player must wait a few seconds for eye to follow,
making immediate area clearer and distant spaces obscured.
*/
class Fog extends Weather {
  makeDiv() {
    this.div = makeWeatherLayer('fog');
    this.div.classList.add('fog');
    grid.after(this.div);
    //portal.insertBefore(this.div,darkness);
  }
  recenter(incremental) {
    var dudex = pixelX(game.dude.slot),
        dudey = pixelY(game.dude.slot);
    if (incremental && this.lastCenter) {
      this.lastCenter = {
        // move halfway or 1 space (whichever is less) toward player
        x:lerp(this.lastCenter.x, dudex, .5, spaceWidth),
        y:lerp(this.lastCenter.y, dudey, .5, spaceWidth)
      }
    } else {
      this.lastCenter = {x:dudex, y:dudey};
    }
    return `${this.lastCenter.x}px ${this.lastCenter.y}px`;
  }
  refresh(lvl,incremental) {
    var light = lerpInteger(0,255,lvl),   //whiteness at eye center (0 to 255)
        opacity = .8*lvl,                 //opacity at eye center (0 to .8)
        radius = percentString(1-.7*lvl,'%'), //radius of eye (100% to 30%)
        center = this.recenter(incremental),
        gradient =
          `radial-gradient(circle at ${center}, rgba(${light},${light},${light},${opacity}), white ${radius})`;
    this.div.style['-webkit-mask'] = gradient;
  }
  adjustLevel(lvl) {
    if (lvl>=0 && lvl<=1) {
      this.refresh(lvl);
      this.temperature = lerp(45,32,lvl); //HACK: fog exists between 32 and 45 degress F
      this.dampness = makeDampness(0,lvl*8,0);
      this.div.style.background = 'white';

      // visual calibration: non-linear function boosts visibility of low levels
      this.div.style.opacity = Math.pow(lvl,.33); //currently: cube root of lvl
    }
  }

  // Verson 1 (follow player moves):
  animate() {}
  updateLocation() {
    if (this.div)
      this.refresh(this.level);
  }

  // Version 2 (converge slowly toward player):
  /*
  updateLocation() {}
  animate() {
    var me = this;
		function refresh() {
      me.refresh(me.level,'incremental');
		}
		animateHelper(me.div,800,0,refresh);
  }
  */
}

class Rain extends Weather {
	makeDiv() {
		this.div = makeWeatherLayer('rain2');
		this.div.classList.add('rain');
    grid.after(this.div);
		//portal.insertBefore(this.div,darkness);
	}
	adjustLevel(lvl) {
		if (lvl>=0 && lvl<=1) {
      this.temperature = lerp(70,45,lvl);//HACK: rain exists between 45 and 70 deg F
      this.dampness = makeDampness(lvl*10,lvl*2,lvl*3);
      var angle = 95.33; //needs to meet in phase when wrapping around top<->bottom
			var gradient =
        //`repeating-linear-gradient(${angle}deg, rgba(0,0,0,.6) 10px, transparent 12px, rgba(0,0,0,${lvl}) 15px)`;
        `repeating-linear-gradient(${angle}deg, black 10px, transparent 15px, black 15px)`;
			this.div.style['-webkit-mask']= gradient;
      this.div.style.opacity = Math.sqrt(lvl);
      //this.div.style.background = gradient; //make visible for debugging
		}
	}
	animate() {
		var mydiv = this.div;
		function drip(n) {
			mydiv.style['-webkit-mask-position-x'] = n+'px';
			mydiv.style.backgroundPosition = 'center '+2*n+'px';
		}
		animateHelper(mydiv,120,2,drip);
	}
}

class Snow extends Weather {
	makeDiv() {
		var plane1 = makeWeatherLayer('snow2'),
				plane2 = makeWeatherLayer('snow6'),
				plane3 = makeWeatherLayer('snow5');
		this.div = makeWeatherLayer('snow');
		this.div.classList.add('snow');
		this.div.appendChild(plane1);
		this.div.appendChild(plane2);
		this.div.appendChild(plane3);
    grid.after(this.div);
		//portal.insertAfter(this.div,grid);
	}
	adjustLevel(lvl) {
		if (lvl>=0 && lvl<=1) {
      this.temperature = lerp(35,25,lvl); // HACK: snow exists between 25-35 deg F
      this.dampness = makeDampness(lvl*4,lvl*2,lvl*lvl*10);

			this.div.children[0].style.opacity = ramp(lvl,0,.2);
			this.div.children[1].style.opacity = ramp(lvl,.1,.4);
			this.div.children[2].style.opacity = ramp(lvl,.3,.7);
      //var portal = document.getElementById('portal');
      portal.style.setProperty('--snow-lvl',lvl || '');
      grid.classList.toggle('snowy',lvl>0);
		}
	}
	animate() {
		var plane1 = snow.children[0],
				plane2 = snow.children[1],
				plane3 = snow.children[2];
		function fall(n) {
			plane1.style.backgroundPosition = vectorY(n,   n/3);
			plane2.style.backgroundPosition = vectorY(n*2,-n/4);
			plane3.style.backgroundPosition = vectorX(n*3, n/5);
		}
		animateHelper(this.div,120,1,fall);
	}
}

Weather.types = {
  rain : Rain,
  snow : Snow,
  fog : Fog
};

function getTemperature() {
  if (weather && weather.div)
    return weather.temperature;
  else
    return Math.round(Math.random()*10)+60; //60-70 deg F
}

function makeDampness(p,h,g) {
  return {
    precipitation:p, // water falling down
    humidity:h, // water saturating the air
    ground:g // wetness of ground
  }
}
function getDampness() {
  if (weather && weather.div) //not paused
    return weather.dampness;
  else
    return makeDampness(0,0,0);
}

function clamp(input,low,high) { // ensure that input is between low and high
  if (input<=low) return low;
  if (input>=high) return high;
  return input;
}

function ramp(input,low,high) { // return 0..1, what fraction input is between low and high
	if (input<=low) return 0;
	if (input>=high) return 1;
	var range = high-low;
	return (input-low)/range;
}

function vectorY(n,o) {
	return o+'px '+n+'px';
}
function vectorX(n,o) {
	return n+'px '+o+'px';
}
function vector(n) {
	return n+'px';
}
function animateHelper(div,msec,increment,changeFn) {
	if (div.timer)
		stopAnimating(div);

	var counter=0;

	function tick() {
		var n = (counter+=increment);//%480;
		changeFn(n);
	}
	div.timer = window.setInterval(tick,msec);
}

function stopAnimating(div) {
	if (div.timer >=0)
		window.clearInterval(div.timer);
	delete div.timer;
}


// Just for fun:
function lightningFlash() {
	darkness.style.display = 'none';
	window.setTimeout(function() {darkness.style.display = ''}, 100);
}

/*--- GUI ---*/

var   weatherTargetMark,
      activeWeatherButton;

/*
function displayWeatherStatus(type,lvl) {
	var sign = (type==='rain')? -1: 1,
			scale = 56,
			xorigin = 56,
			x = sign * lvl * scale + xorigin;
	weatherTargetMark.style.left = x+'px';
}
*/

function refreshButtonStatus(div,lvl) {
  if (div) {
    div.style.setProperty('--weather-lvl', lvl? percentString(lvl) : '');
    div.classList.toggle('active',lvl>0);
  }
}

function displayWeatherStatus(type,lvl) {
  // run at each tick of a transition; adjust level shown on active button
  var div = document.getElementById(weatherButtonId(type));
  if (div!==activeWeatherButton) {
    //switch active button
    refreshButtonStatus(activeWeatherButton,0);
    activeWeatherButton = div;
  }
  refreshButtonStatus(activeWeatherButton,lvl);
}

function displayWeatherTarget(type,lvl,button) {
  // run at the start of a transition; moves white marker to show pending weather
  if (!button)
    button = document.getElementById(weatherButtonId(type));
  // move white line marker to correct button:
  button.appendChild(weatherTargetMark);
  // set its vertical position:
  weatherTargetMark.style.setProperty('--target-lvl',percentString(lvl,'%'));
}

function relativeY(evt) {
  return 1-(evt.offsetY-1)/40; //buttons are 40px high
}

function weatherButtonId(type) {
  return `weatherButton_${type}`;
}

function makeWeatherButton(box,type,icon) {
	var button = document.createElement('div');
	button.className = 'weatherButton '+ icon;
  button.id = weatherButtonId(type);
	button.onclick = function(evt) {
		evt.target.blur();
    // calculate click's vertical position within icon, which determines new weather level:
    var lvl = (type==='clear')? 1: relativeY(evt);
    //steal shared target marker
    displayWeatherTarget(type,lvl,evt.target);
		transitionWeather(type,lvl);
	};
	box.appendChild(button);
}

function makeDaylightToggle(box) {
  var button = document.createElement('div');
  button.id =("daylightToggle");
  button.onclick = function(evt) {
    box.classList.toggle('nightTheme');
    toggleDaylight(box);
  }
  box.appendChild(button);
}

function makeWeatherControls() {
	var toprow = document.getElementById('toprow-left'),
			controls = document.createElement('div');
	weatherTargetMark = document.createElement('div');
  weatherTargetMark.id = "weatherTargetMark";
  controls.id = "weatherControls";
  controls.classList.add("nightTheme");
  makeDaylightToggle(controls);
  makeWeatherButton(controls,'clear','clearsky');
	makeWeatherButton(controls,'rain','raindrop');
	makeWeatherButton(controls,'snow','snowflake');
  makeWeatherButton(controls,'fog','fog');
	toprow.appendChild(controls);
}

// Public interface.  Only these functions are available outside this module:
return {
  transitionWeather:transitionWeather,
  makeWeatherControls:makeWeatherControls,
  resumeWeather:resumeWeather,
  pauseWeather:pauseWeather,
  getTemperature:getTemperature,
  getDampness:getDampness
}

})();
